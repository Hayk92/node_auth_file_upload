'use strict';
const bcrypt = require("bcrypt");
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      models.User.hasMany(models.File, {
        foreignKey: "userId",
        onUpdate: "CASCADE",
        onDelete: "SET NULL"
      })
    }
  };
  User.init({
    email: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User',
    hooks: {
      beforeCreate: function(createdUser, options, cb) {
        createdUser.password = bcrypt.hashSync(createdUser.password, bcrypt.genSaltSync(+process.env.HASH_SALT));
      }
    },
    instanceMethods: {
      generateHash(password) {
        return bcrypt.hash(password, bcrypt.genSaltSync(process.env.HASH_SALT));
      },
      validPassword(password) {
        return bcrypt.compare(password, this.password);
      }
    }
  });
  return User;
};