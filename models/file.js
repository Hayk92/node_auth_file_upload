'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class File extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      models.File.belongsTo(models.User, {
        foreignKey: "id",
        onDelete: "CASCADE"
      })
    }
  };
  File.init({
    path: DataTypes.STRING,
    type: DataTypes.STRING,
    name: DataTypes.STRING,
    extention: DataTypes.STRING,
    userId: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'File',
  });
  return File;
};