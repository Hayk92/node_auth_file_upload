const AuthController = require('./auth.controller'),
    FileController = require('./file.controller');

module.exports = {
    AuthController,
    FileController
};
