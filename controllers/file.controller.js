const {File} = require('../models/index'),
    fs = require('fs');

module.exports = {
    /*
    * @params {req, res}
    * @return {file}
    * */
    getFile: (req, res, next) => {
        File.findOne({where: {id: req.params.id, userId: req.user.id}})
            .then(file => {
                if (!file)
                    return res.status(404).json({message: "File not found!"});

                return res.status(200).json(file);
            })
            .catch(err => {
                return res.status(404).json({message: "File not found!"});
            });
    },
    /*
    * @params {req, res}
    * @return {files}
    * */
    getFiles: (req, res, next) => {
        File.findAll({where: {userId: req.user.id}})
            .then(files => {
                return res.status(200).json(files);
            })
    },
    /*
    * @params {req, res}
    * @return {message}
    * */
    deleteFile: (req, res, next) => {
        File.findOne({where: {id: req.params.id, userId: req.user.id}})
            .then(file => {
                fs.unlink(`./${file.path}`, () => {
                    File.destroy({where: {id: req.params.id, userId: req.user.id}})
                        .then(() => {
                            return res.status(200).json({message: "File has been deleted"});
                        })
                        .catch(err => {
                            return res.status(400).json({message: "Bad request"});
                        })
                })
            })
            .catch(err => {
                return res.status(400).json({message: "Bad request"});
            })
    },
    /*
    * @params {req, res}
    * @return {boolean: true/false}
    * */
    updateFile: (req, res, next) => {
        if (!req.files)
            return res.status(400).json({message: "No file uploaded"});

        File.findOne({where: {id: req.params.id, userId: req.user.id}})
            .then(file => {
                fs.unlink(`./${file.path}`, () => {
                    let file = req.files.file;
                    file.mv(`./uploads/${req.user.id}/${file.name}`);
                    File.update({
                        path: `/uploads/${req.user.id}/${file.name}`,
                        type: file.mimetype,
                        name: file.name,
                    }, {
                        where: {
                            id: req.params.id,
                            userId: req.user.id
                        }
                    })
                        .then(file => {
                            return res.status(200).json(file);
                        })
                        .catch(err => {
                            return res.status(400).json({message: "Bad request"});
                        })
                })
            })
            .catch(err => {
                return res.status(404).json({message: "File not found"});
            })
    },
    /*
    * @params {req, res}
    * @return {file}
    * */
    uploadFile: (req, res, next) => {
        try {
            if (!req.files)
                return res.status(400).json({message: "No file uploaded"});

            let file = req.files.file;

            file.mv(`./uploads/${req.user.id}/${file.name}`);

            File.create({
                path: `/uploads/${req.user.id}/${file.name}`,
                type: file.mimetype,
                name: file.name,
                userId: req.user.id
            }).then(file => {
                return res.status(200).json(file);
            })

        } catch (e) {
            return res.status(400).json({message: "No file uploaded"});
        }
    },
    /*
    * @params {req, res}
    * @return {image}
    * */
    downloadFile: (req, res, next) => {
        File.findOne({where: {id: req.params.id, userId: req.user.id}})
            .then(file => {
                res.download(`./${file.path}`);
            })
            .catch(err => {
                return res.status(404).json({message: "File not found"});
            })
    }
};
