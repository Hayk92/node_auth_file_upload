const {User} = require('../models'),
    bcrypt = require('bcrypt'),
    passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    passportJWT = require('passport-jwt'),
    JWTStrategy = passportJWT.Strategy,
    ExtractJWT = passportJWT.ExtractJwt;

require("dotenv").config();

passport.serializeUser((user, done) => {
    done(null, user.id);
});

passport.deserializeUser((id, done) => {
    User.findOne({
        where: {id}
    }).then(user => {
        done(null, user);
    }).catch(err => {
        done(err);
    })
});

passport.use(
    new JWTStrategy({
        jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
        secretOrKey: process.env.SESSION_SECRET
    }, function (jwtPayload, cb) {
        User.findOne({
            where: {id: jwtPayload.id}
        }).then(user => {
            return cb(null, user);
        }).catch(err => {
            return cb(err);
        })
    })
);

passport.use(
    new LocalStrategy(
        {usernameField: "email"},
        (email, password,  done) => {
            User.findOne({where: {email: email.toLowerCase()}})
                .then(user => {
                    if (!user) return done(null, false, "Invalid Credentials");
                    return bcrypt.compareSync(password, user.password)?done(null, user):done(null, false, "Invalid Credentials");
                })
                .catch(err => {
                    return done(err);
                })
        }
    )
);