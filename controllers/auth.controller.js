const {User} = require('../models/index'),
    passport = require("passport"),
    jwt = require("jsonwebtoken");

require('../services/auth');

const tokenList = {};

module.exports = {
    /*
    * @params {req, res}
    * @return {user}
    * */
    login: async (req, res, next) => {
        passport.authenticate('local', {session: false}, (err, user, info) => {
            console.log(user, err)
            if (err || !user) {
                return res.status(400).json({
                    message: 'Something is not right',
                    user: user
                });
            }

            req.login(user, {session: false}, async err => {
                if (err) return res.send(err);
                const token = jwt.sign(
                    {id: user.id},
                    process.env.SESSION_SECRET,
                    {expiresIn: "10 minutes"}
                );

                await User.update({token: token}, {where: {id: user.id}});
                const refreshToken = jwt.sign({id: user.id}, process.env.REFRESH_TOKEN_SECRET, {expiresIn: "30 days"});

                tokenList[refreshToken] = {token, refreshToken};

                return res.status(200).json({token, refreshToken});
            })
        })(req, res);
    },

    /*
    * @params {req, res}
    * @return {user}
    * */
    register: async (req, res, next) => {
        if (!req.body.email || !req.body.password)
            return res.status(422).json({errors: {email: "Bad email or password"}});

        User.findOne({where: {email: req.body.email}})
            .then(async user => {
                if (user)
                    return res.status(406).send({ error: 'User already exists' });
                try {
                    User.create({
                        email: req.body.email,
                        password: req.body.password
                    }).then(user => {
                        passport.authenticate('local', {session: false})(
                            req, res, async function () {
                                const token = jwt.sign(
                                    {id: user.id},
                                    process.env.SESSION_SECRET,
                                    {expiresIn: "10 minutes"}
                                );
                                await User.update({token: token}, {where: {id: user.id}});
                                const refreshToken = jwt.sign({id: user.id}, process.env.REFRESH_TOKEN_SECRET, {expiresIn: "30 days"});

                                tokenList[refreshToken] = {token, refreshToken};
                                return res.status(200).json({ token, refreshToken });
                            }
                        )
                    }).catch(err => {
                        console.log(err);
                    })
                } catch (e) {
                    res.status(422).send(e);
                }
            })
            .catch(res.negotiate);
    },
    /*
    * @params {req, res}
    * @return {token}
    * */
    refreshToken: async (req, res, next) => {
        if (req.body.refreshToken && (req.body.refreshToken in tokenList)){
            const token = jwt.sign({id: req.user.id}, process.env.SESSION_SECRET, {expiresIn: "10 minutes"});
            tokenList[req.body.refreshToken].token = token;

            return res.status(200).json({ token });
        }
        return res.status(404).json({errors: {message: 'Invalid request'}})
    },
    /*
    * @params {req, res}
    * @return {message}
    * */
    logout: async (req, res, next) => {
        req.logout();
        res.status(200).json({message: "Logged out"});
    },
    /*
    * @params {req, res}
    * @return {email}
    * */
    info: async (req, res, next) => {
        res.status(200).json({email: req.user.email});
    }
};