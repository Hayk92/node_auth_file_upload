const express = require("express"),
    route = express.Router(),
    passport = require("passport"),
    { AuthController } = require("../controllers");

require('../services/auth');

route.post('/signup', AuthController.register)
    .post('/signin', AuthController.login)
    .post('/signin/new_token', passport.authenticate('jwt', { session: false }), AuthController.refreshToken)
    .get('/info', passport.authenticate('jwt', { session: false }), AuthController.info)
    .get('/logout', passport.authenticate('jwt', { session: false }), AuthController.logout);

module.exports = route;
