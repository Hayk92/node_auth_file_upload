const express = require("express"),
    fileUpload = require("express-fileupload"),
    cors = require("cors"),
    bodyParser = require("body-parser"),
    jsonParser = bodyParser.json();

const PORT = process.env.PORT || 3000;

const {
    auth,
    file
} = require('./routes');

let app = express();

app.use(fileUpload({
    createParentPath: true,
    limits: {
        fileSize: 2 * 1024 * 1024 * 1024
    },
}));

app.use(jsonParser);
app.use(cors());
app.use(express.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json({ type: 'application/*+json' }));

app.use('/api/v1/auth', auth);
app.use('/api/v1/file', file);

app.listen(PORT, () => console.log(`listening on port ${PORT}`));