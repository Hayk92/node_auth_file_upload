const express = require("express"),
    route = express.Router(),
    passport = require("passport"),
    {FileController} = require('../controllers');

require('../services/auth');

route.get("/list", passport.authenticate('jwt', { session: false }), FileController.getFiles)
    .post("/upload",passport.authenticate('jwt', { session: false }), FileController.uploadFile)
    .delete("/delete/:id", passport.authenticate('jwt', { session: false }), FileController.deleteFile)
    .get("/:id", passport.authenticate('jwt', { session: false }), FileController.getFile)
    .get("/download/:id", passport.authenticate('jwt', { session: false }), FileController.downloadFile)
    .put("/update/:id", passport.authenticate('jwt', { session: false }), FileController.updateFile);

module.exports = route;
